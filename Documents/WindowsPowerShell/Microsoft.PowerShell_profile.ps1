# init
chcp 65001
clear
D:/Terminal/oh-my-posh/bin/oh-my-posh.exe init pwsh -c D:/Terminal/oh-my-posh/themes/montys.omp.json | invoke-expression

# variable
$p = $profile
$g = 'D:/Terminal/git'
$VIM = 'D:/Terminal/vim/'
$vimrc = 'D:/Terminal/vim/_vimrc'
$c = 'C:/'
$d = 'D:/'
$e = 'E:/'
$f = 'F:/'
$hosts = 'C:\Windows\System32\drivers\etc\hosts'

# function
function cg {cd $g/}
function vp {vim $profile}
function vv {vim $vimrc}
function cv {cd $VIM}
function reboot {shutdown -r -t 0}
function shutdown {shutdown -s -t 0}
#function pnet {$args | foreach{telnet 192.168.200.200 3000$($_) }}
function pnet {
	if($args -lt 10)
	{
		$args | foreach{telnet 192.168.200.200 3000$($_)}
	}
	elseIf ($args -lt 100)
	{
		$args | foreach{telnet 192.168.200.200 300$($_)}
	}
	else 
	{
		$args | foreach{telnet 192.168.200.200 30$($_)}
	}
}

# Alias
New-Alias -Name py -Value python
New-Alias -Name tel -Value telnet

#key
~/Documents/Powershell/key.ps1
